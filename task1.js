function mood(a, point) {
  if(a % point == 0) {
    return true;
  }
}

function containsFour(a) {
  return /[4]/.test(a);
}

function containsEight(a) {
  return /[8]/.test(a);
}

function digitSum(a, point) {
  if(a.toString().split('').map(Number).reduce(function (x, y) {
    return x + y;
  }, 0) % point == 0)
  return true;
}

for (i = 1; i <= 100; i++) {
  if ((mood(i, 4) || containsFour(i) || digitSum(i, 4)) && (mood(i, 8) || containsEight(i) || digitSum(i, 8))) {
    console.log(i + " -> bulldog");
  } else if (mood(i, 4) || containsFour(i) || digitSum(i, 4)) {
    console.log(i + " -> bull");
  } else if (mood(i, 8) || containsEight(i) || digitSum(i, 8)) {
    console.log(i + " -> dog");
  }
  else {console.log(i)}
}

