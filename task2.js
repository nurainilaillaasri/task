function base(num) {
  let a;
  let result = [];

  while (num !== 0) {
    a = num % 6;
    num = (num - a) / 6;
    result = [a, ...result];
  }
  return result.join(' ');
}

console.log(base(100))
